const express = require("express")
const container = require("./container")

const app = express()

app.get("/", (req,res) => {
    return res.status(200).send("oke")
})
app.get("/info", (req,res) => {
    return res.status(200).send("oke")
})
app.use("/api/customers", container.customerRouter)

module.exports = app