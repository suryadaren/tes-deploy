// init database
const db = require("../../models")

// init all services
const customerService = require("../services/customerService")(db)

// init all controller
const customerController = require("../controllers/customerController")(customerService)

// init all router
const customerRouter = require("../routers/customerRouter")(customerController)

module.exports = {
    customerRouter
}