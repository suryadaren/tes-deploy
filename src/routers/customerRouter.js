const {Router} = require("express")

module.exports = (controller) => {
    const router = Router()
    router.get("/customers", controller.fetchCustomers)
    return router
}