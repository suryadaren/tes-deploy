'use strict';
const faker = require("faker")

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    const dataCustomer = []
    const dataAddress = []

    for (let i = 1; i < 11; i++) {
      dataCustomer.push({
        name : faker.name.findName(),
        addressId : i,
        createdAt : new Date(),
        updatedAt : new Date(),
      })
      dataAddress.push({
        name : faker.address.streetAddress(),
        createdAt : new Date(),
        updatedAt : new Date(),
      })  
    }
    
    await queryInterface.bulkInsert('addresses', dataAddress);
    await queryInterface.bulkInsert('customers', dataCustomer);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('customers', null, {});
    await queryInterface.bulkDelete('addresses', null, {});
  }
};
